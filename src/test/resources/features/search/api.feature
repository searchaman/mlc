Feature: Australia Post API

  @cucumber
  Scenario Outline: Shipping Calculation Endpoint
    Given The API is running
    When User hit the API with "<weight>"
    Then The API returns the shipping cost as "<expected>"

    Examples:
      | weight | expected |
      | 5      | 10       |

