Feature: MLC Insurance Test

  @cucumber
  Scenario: Searching for Lifeview Link on Home page
    Given  User is on MLC Insurance Home page
    When he searches for "Lifeview"
    Then Breadcrumb displayed is "Home Partnering with us Superannuation funds LifeView"
#    When user clicks on Request Demo
#    Then  user fills the Demo form with name "ABC", company "xyz pty ltd", email "email@fff.com" and phone "0415555555"

