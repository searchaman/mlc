Feature: ATO

  @cucumber
  Scenario Outline: Calculation of Income Tax
    Given User is on ATO Home page
    When user selects an income year "<year>"
    And user enters Total income "<totalIncome>"
    And user selects Residency status as "<residencyStatus>"
    And user clicks on Submit button
    Then Taxable income is displayed

    Examples:
      | year | totalIncome | residencyStatus            |
      | 2018 | 80000       | Resident for Full Year     |
      | 2019 | 70000       | Non-resident for Full Year |
      | 2016 | 100000      | Resident for Full Year     |

