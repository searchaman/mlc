package starter.navigation;

import net.thucydides.core.annotations.Step;

public class NavigateAtoHome {

    AtoHomePage atoHomePage;

    @Step("Open the ATO home page")
    public void theatoHomePage() {
        atoHomePage.open();
    }
}
