package starter.navigation;

import net.thucydides.core.annotations.Step;

public class NavigateHome {

    MlcInsuranceHomePage mlcInsuranceHomePage;

    @Step("Open the MLC Insurance home page")
    public void themlcinsuranceHomePage() {
        mlcInsuranceHomePage.open();
    }
}
