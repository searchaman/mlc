package starter.search;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class BreadCrumbs extends UIInteractionSteps {

    @Step("Verify Bread Crumbs")
    public String crumb1() {
        return $(BreadcrumbsForm.CRUMB1).getText();
    }

    public String crumb2() {
        return $(BreadcrumbsForm.CRUMB2).getText();
    }

    public String crumb3() {
        return $(BreadcrumbsForm.CRUMB3).getText();
    }


}