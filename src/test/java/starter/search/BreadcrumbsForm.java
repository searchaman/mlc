package starter.search;

import org.openqa.selenium.By;

class BreadcrumbsForm {
    static By CRUMB1 = By.xpath("//li[@class='home']");
    static By CRUMB2 = By.xpath("//div[@class='breadcrumbs']//li[2]");
    static By CRUMB3 = By.xpath("//div[@class='breadcrumbs']//li[3]");
}
