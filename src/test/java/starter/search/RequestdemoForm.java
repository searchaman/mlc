package starter.search;

import org.openqa.selenium.By;

class RequestdemoForm {
//    static By SCROLLDOWN = Actor.attemptsTo
    static By DEMO_BUTTON = By.xpath("//*[text()='Request a Demo']");
    static By NAME = By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_0__Value");
    static By COMPANY = By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_1__Value");
    static By EMAIL = By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_2__Value");
    static By PHONENO = By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_3__Value");
}
