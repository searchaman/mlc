package starter.search;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;

public class SearchFor extends UIInteractionSteps {

    @Step("Search for Lifeview")
    public void term(String term) {
        $(SearchForm.SEARCH_FIELD).click();
        $(SearchForm.SEARCH_TEXT).sendKeys("Lifeview");
        $(SearchForm.SEARCH_BUTTON).click();
    }
}
