package starter.search;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.JavascriptExecutor;


public class RequestDemo extends UIInteractionSteps {

    @Step("Fill the Form")
    public void req() {
//        JavascriptExecutor js = (JavascriptExecutor) webdriver;
//        js.executeScript("window.scrollBy(0,1000)");
        $(RequestdemoForm.DEMO_BUTTON).click();
    }

    @Step("Fill the Form")
    public void fillForm(String name, String company, String email, String phone) {
        $(RequestdemoForm.NAME).sendKeys(name);
        $(RequestdemoForm.COMPANY).sendKeys(company);
        $(RequestdemoForm.EMAIL).sendKeys(email);
        $(RequestdemoForm.PHONENO).sendKeys(phone);

    }
}
