package starter.search;

import org.openqa.selenium.By;

class SearchForm {
    static By SEARCH_FIELD = By.xpath("//button[@aria-label='Toggle search']");
    static By SEARCH_TEXT = By.id("q");
    static By SEARCH_BUTTON = By.className("autocomplete-results-item-title");
}
