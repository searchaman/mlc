package starter.api;

public enum AppStatus {
    RUNNING, DOWN
}
