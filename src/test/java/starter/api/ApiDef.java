package starter.api;

import groovy.json.JsonBuilder;
import groovy.json.JsonSlurper;
import net.serenitybdd.rest.SerenityRest;
import org.json.simple.JSONObject;


import net.thucydides.core.annotations.Step;

public class ApiDef {

    @Step("Post the api")
    public void postApi(String weight) {
        Object Obj = new JsonSlurper().parseText(this.getClass().getResource("/apibody/body.json").toString());
        JsonBuilder builder = new JsonBuilder(new JsonSlurper().parseText(this.getClass().getResource("/apibody/body.json").toString()));

        JSONObject apiBody = (JSONObject) Obj;
        apiBody.put("weight",weight);

        SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json", "Accept", "application/json", "Account-Number", "0001234534", "Authorization", "Basic eW91cl91c2VyX25hbWU6cGFzc3dvcmQ=")
                .body(apiBody)
                .when()
                .post("https://digitalapi.auspost.com.au/shipping/v1/prices/shipments");
    }
}
