package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;
import starter.navigation.NavigateAtoHome;
import starter.atoquestions.AtoCalculateTax;

public class AtoStepDefinitions {

    @Steps
    NavigateAtoHome navigateAtoHome;

    @Steps
    AtoCalculateTax atoCalculateTax;



    @Given("^User is on ATO Home page$")
    public void user_is_on_ato_home_page()  {
        navigateAtoHome.theatoHomePage();
    }

    @When("^user selects an income year \"([^\"]*)\"$")
    public void user_selects_an_income_year(String year) {
        atoCalculateTax.year(year);
    }

    @Then("^Taxable income is displayed$")
    public void taxable_income_is_displayed() {
        atoCalculateTax.taxEstimateText();
    }

    @And("^user enters Total income \"([^\"]*)\"$")
    public void user_enters_total_income(String inc) {
        atoCalculateTax.income(inc);
    }

    @And("^user selects Residency status as \"([^\"]*)\"$")
    public void user_selects_residency_status_as(String residency) {
        atoCalculateTax.residency(residency);
    }

    @And("^user clicks on Submit button$")
    public void user_clicks_on_submit_button() {
        atoCalculateTax.submit();
    }
}
