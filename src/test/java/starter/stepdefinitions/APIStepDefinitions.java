package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;


import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import starter.api.ApiDef;


public class APIStepDefinitions {

    @Steps
    ApiDef apiDef;

    @Given("^The API is running$")
    public void api_running() {

    }

    @When("^User hit the API with \"([^\"]*)\"$")
    public void user_hit_api(String weight) {

        apiDef.postApi(weight);
    }

    @Then("^The API returns the shipping cost as \"([^\"]*)\"$")
    public void taxable_income_is_displayed(String expectedMessage) {
        restAssuredThat(response -> response.statusCode(200));
    }
}
