package starter.stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.search.SearchFor;
import starter.search.RequestDemo;
import starter.navigation.NavigateHome;
import starter.search.BreadCrumbs;
import static org.assertj.core.api.Assertions.assertThat;

public class SearchOnLifeviewStepDefinitions {

    @Steps
    NavigateHome navigateHome;

    @Steps
    SearchFor searchFor;

    @Steps
    BreadCrumbs breadCrumbs;

    @Steps
    RequestDemo requestDemo;


    @Given("User is on MLC Insurance Home page")
    public void user_is_on_mlc_insurance_home_page() {
        navigateHome.themlcinsuranceHomePage();
    }

    @When("^he searches for \"([^\"]*)\"$")
    public void he_searches_for(String term) {
        searchFor.term(term);
    }

    @Then("^Lifeview page is displayed$")
    public void lifeview_page_is_displayed() {
    }

    @Then("^Breadcrumb displayed is \"([^\"]*)\"$")
    public void breadcrumb_displayed_is(String breadcrumb) {
       String bc1=  breadCrumbs.crumb1();
       String bc2=  breadCrumbs.crumb2();
       String bc3=  breadCrumbs.crumb3();
       breadcrumb.equals(bc1 + " " + bc2 + " " + bc3);

    }

    @When("^user clicks on Request Demo$")
    public void user_clicks_on_Request_Demo() {
        requestDemo.req();
    }

    @Then("^user fills the Demo form with name \"([^\"]*)\", company \"([^\"]*)\", email \"([^\"]*)\" and phone \"([^\"]*)\"$" )
    public void user_fills_the_Demo_form(String name, String company, String email, String phone) {
        requestDemo.fillForm(name, company, email, phone);

    }

}
