package starter.atoquestions;

import org.openqa.selenium.By;

class AtoCalculateTaxForm {
    static By year =  By.id("ddl-financialYear");
    static By income = By.id("texttaxIncomeAmt");
    static By residency1 = By.id("vrb-resident-span-0");
    static By residency2 = By.id("vrb-resident-span-1");
    static By residency3 = By.id("vrb-resident-span-2");
    static By submitButton = By.id("bnav-n1-btn4");
    static By taxEstimateText = By.xpath("//p[contains(text(),'The estimated tax on your taxable income is')]");
}
