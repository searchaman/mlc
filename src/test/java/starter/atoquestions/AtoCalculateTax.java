package starter.atoquestions;

import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;


public class AtoCalculateTax extends UIInteractionSteps {

    @Step("Income Tax calculation")
    public void year(String year) {
        $(AtoCalculateTaxForm.year).selectByValue(year);
    }

    public void income(String income) {

        $(AtoCalculateTaxForm.income).sendKeys(income);
    }


    public void residency(String residency) {
        if (residency.equals("Resident for Full Year")) {
            $(AtoCalculateTaxForm.residency1).click();
        } else if (residency.equals("Non-resident for Full Year")) {
            $(AtoCalculateTaxForm.residency2).click();
        } else
            $(AtoCalculateTaxForm.residency3).click();
    }


    public void submit() {

        $(AtoCalculateTaxForm.submitButton).click();
    }

    public void taxEstimateText() {

        $(AtoCalculateTaxForm.taxEstimateText).isPresent();
    }
}
